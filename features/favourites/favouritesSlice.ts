import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import type { RootState } from '../../app/store'
import type { Character } from '../../src/interfaces'
import * as Constants from '../../constants'

export type FavouritesState = {
  value: Array<any>
}

const initialState: FavouritesState = {
  value: typeof window !== 'undefined' && 'localStorage' in window && window.localStorage.getItem(Constants.FAVOURITES_KEY) ? JSON.parse(window.localStorage.getItem(Constants.FAVOURITES_KEY)) : []
}

export const favouritesSlice = createSlice({
  name: Constants.FAVOURITES_KEY,
  initialState,
  reducers: {
    add: (state, action: PayloadAction<Array<Character>>) => {
      state.value = [...state.value, ...action.payload]
      if ('localStorage' in window) {
        localStorage.setItem(Constants.FAVOURITES_KEY, JSON.stringify(state.value))
      }
    },
    addOnlyStore: (state, action: PayloadAction<Array<Character>>) => {
      state.value = [...state.value, ...action.payload]
    },
    remove: (state, action: PayloadAction<string>) => {
      const characterIndex: number = state.value.findIndex(({ name }) => name === action.payload)
      if (characterIndex > -1) {
        state.value.splice(characterIndex, 1)
      }

      if ('localStorage' in window) {
        localStorage.setItem(Constants.FAVOURITES_KEY, JSON.stringify(state.value))
      }
    }
  },
})

export const { add, addOnlyStore, remove } = favouritesSlice.actions
export const selectFavourites = (state: RootState) => state.favourites.value
export default favouritesSlice.reducer