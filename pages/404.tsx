import type { NextPage } from 'next'

const Page404: NextPage = () => {
  return (
    <div className="text-center pt-5">
      <h1>
        Oooops... page not found
      </h1>
      <a href="/" className="d-inline-block mt-3">Back to Home Page</a>
    </div>
  )
}

export default Page404