import type { NextPage } from 'next'
import { useAppSelector } from '../../app/hooks'
import { selectFavourites } from '../../features/favourites/favouritesSlice'
import { Character } from '../../src/interfaces'

import MainTitle from '../../src/components/common/main-title'
import SimpleCharacterList from '../../src/components/common/simple-character-list'

const Favourites: NextPage = () => {
  const favouritesList = useAppSelector(selectFavourites)

  const getFavouritesContent = (favouritesList: Array<Character>, fallback: string): any => favouritesList.length ? <SimpleCharacterList collection={favouritesList}></SimpleCharacterList> : <h3>{fallback}</h3>

  return (
    <div className="container">
      <div className="row">
        <div className="col-md-4">
          <MainTitle content="Favourites" />
          <p>Lucas ipsum dolor sit amet darth hutt kessel naboo jabba darth moff gamorrean tatooine han. Moff bespin yavin amidala kamino darth mon antilles. Jango owen sidious ackbar. Boba organa boba ben. Darth skywalker amidala darth ackbar moff ackbar aayla. Vader mon ventress boba hutt. Endor darth mara ewok skywalker chewbacca sidious. Jango darth zabrak luuke. Gamorrean calrissian windu c-3p0 hutt. Maul dantooine luuke calrissian yoda skywalker maul alderaan yoda. Hutt ventress qui-gonn organa moff dantooine jabba.</p>
        </div>
        <div className="col-md-8">
          {getFavouritesContent(favouritesList, 'Brak ulubionych')}
        </div>
      </div>
    </div>
  )
}

export default Favourites
