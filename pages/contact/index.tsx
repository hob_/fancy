import type { NextPage } from 'next'

import ContactForm from '../../src/components/forms/ContactForm'
import MainTitle from '../../src/components/common/main-title'

const Contact: NextPage = () => {
  return (
    <div className="container">
      <div className="row">
        <div className="col-md-4">
          <MainTitle content="Contact Form" />
          <p>Lucas ipsum dolor sit amet darth hutt kessel naboo jabba darth moff gamorrean tatooine han. Moff bespin yavin amidala kamino darth mon antilles. Jango owen sidious ackbar. Boba organa boba ben. Darth skywalker amidala darth ackbar moff ackbar aayla. Vader mon ventress boba hutt. Endor darth mara ewok skywalker chewbacca sidious. Jango darth zabrak luuke. Gamorrean calrissian windu c-3p0 hutt. Maul dantooine luuke calrissian yoda skywalker maul alderaan yoda. Hutt ventress qui-gonn organa moff dantooine jabba.</p>
        </div>
        <div className="col-md-8">
          <ContactForm />
        </div>
      </div>
    </div>
  )
}

export default Contact
