import type { NextPage } from 'next'
import { GetServerSideProps, InferGetServerSidePropsType } from 'next'
import { useRouter } from 'next/router'
import { getCharacters } from '../../src/services'
import { Character } from '../../src/interfaces'

import SimpleCharacterList from '../../src/components/common/simple-character-list'
import MainTitle from '../../src/components/common/main-title'
import Pagination from '../../src/components/common/pagination'

export const getServerSideProps: GetServerSideProps = async ({ query: { page = null } }) => {
  try {
    const data: any = await getCharacters(page ? +page : null)
    const characters: Array<Character> = data.results
    return { props: { characters, count: data.count, previous: data.previous, next: data.next } }
  } catch (error) {
    return { props: { characters: [] } }
  }
}

const Characters: NextPage = ({ characters, count, previous, next }: InferGetServerSidePropsType<typeof getServerSideProps>) => {

  const router = useRouter()
  const { page = null } = router.query

  const getCharactersContent = (characterList: Array<Character>, fallback: string): any => characterList.length ? <SimpleCharacterList collection={characterList}></SimpleCharacterList> : <h3>{fallback}</h3>
  const totalPages = (totalResults: number, perPage: number): number => Math.ceil(totalResults / perPage)
  const currentPage: number = page ? +page : 1

  return (
    <div className="container">
      <div className="row">
        <div className="col-md-4">
          <MainTitle content="Character list" />
          <p>Lucas ipsum dolor sit amet darth hutt kessel naboo jabba darth moff gamorrean tatooine han. Moff bespin yavin amidala kamino darth mon antilles. Jango owen sidious ackbar. Boba organa boba ben. Darth skywalker amidala darth ackbar moff ackbar aayla. Vader mon ventress boba hutt. Endor darth mara ewok skywalker chewbacca sidious. Jango darth zabrak luuke. Gamorrean calrissian windu c-3p0 hutt. Maul dantooine luuke calrissian yoda skywalker maul alderaan yoda. Hutt ventress qui-gonn organa moff dantooine jabba.</p>
          <p>Lucas ipsum dolor sit amet darth hutt kessel naboo jabba darth moff gamorrean tatooine han. Moff bespin yavin amidala kamino darth mon antilles. Jango owen sidious ackbar. Boba organa boba ben. Darth skywalker amidala darth ackbar moff ackbar aayla. Vader mon ventress boba hutt. Endor darth mara ewok skywalker chewbacca sidious. Jango darth zabrak luuke. Gamorrean calrissian windu c-3p0 hutt. Maul dantooine luuke calrissian yoda skywalker maul alderaan yoda. Hutt ventress qui-gonn organa moff dantooine jabba.</p>
        </div>
        <div className="col-md-8">
          {getCharactersContent(characters, 'Brak postaci')}
          <div className="my-3">
            <Pagination current={currentPage} total={totalPages(count, 10)} previous={previous} next={next} />
          </div>
        </div>
      </div>
    </div>
  )
}
 
export default Characters
