import { useState } from 'react';
import type { NextPage } from 'next'
import { GetServerSideProps, InferGetServerSidePropsType } from 'next'
import Router from 'next/router'
import { getCharacter, getCharacterHomeworld } from '../../src/services'
import { Character } from '../../src/interfaces'

import MainTitle from '../../src/components/common/main-title'
import BasicModal from '../../src/components/common/basic-modal'

export const getServerSideProps: GetServerSideProps = async ({ res, query: { id } }) => {
  if (!id) {
    res.statusCode = 302
    res.setHeader('Location', `/characters`)
    return {props: {}}
  }
  const idInt: number = +id
  try {
    const character: Character = await getCharacter(idInt)
    return { props: { character } }
  } catch (error) {
    return { props: { character: {}} }
  }
}

const CharacterDetails: NextPage = ({ character }: InferGetServerSidePropsType<typeof getServerSideProps>) => {
  const [homeworld, setHomeworld] = useState({ name: '' })
  const [modalContent, setModalContent] = useState('')
  const [modalOpen, setModalState] = useState(false)

  const showHomeworldModal = async (homeworldUrl: string): Promise<any> => {
    try {
      const homeworld: any = await getCharacterHomeworld(homeworldUrl)
      setHomeworld(homeworld)
      const homeWorldModalContent: Array<string> = Object.entries(homeworld).map(([homeworldKey, homeworldVal]) => `<p class='my-1'><strong>${homeworldKey}</strong> - ${homeworldVal}</p>`)
      setModalContent(homeWorldModalContent.join(''))
      setModalState(true)
    } catch (error) {
      console.log(error)
    }
  }

  const hideHomeworldDetails = (): void => setModalState(false)

  return (
    <div className="container">
      <BasicModal content={modalContent} modalTitle={homeworld.name} open={modalOpen} hideModal={hideHomeworldDetails} />
      <div className="row">
        <div className="col-md-4">
          <MainTitle content={character.name || 'Brak danych do wyświetlenia'} />
          <button onClick={() => Router.back()} className="btn btn-dark">Back</button>
        </div>
        <div className="col-md-8">
          { 
            !Object.keys(character).length
            ?
            null
            :
            <table width="100%">
              <caption style={{ captionSide: 'top' }}>Character detail</caption>
              <colgroup>
                <col width="38%" />
                <col />
              </colgroup>
              <tbody>
                <tr>
                  <th>Name</th>
                  <td>{character.name}</td>
                </tr>
                <tr>
                  <th>Birthday</th>
                  <td>{character.birth_year}</td>
                </tr>
                <tr>
                  <th>Gender</th>
                  <td>{character.gender}</td>
                </tr>
                <tr>
                  <th>Height</th>
                  <td>{character.height}</td>
                </tr>
                <tr>
                  <th>Mass</th>
                  <td>{character.mass}</td>
                </tr>
                <tr>
                  <th>Hair</th>
                  <td>{character.hair_color}</td>
                </tr>
                <tr>
                  <th>Skin</th>
                  <td>{character.skin_color}</td>
                </tr>
                <tr>
                  <th>Eyes</th>
                  <td>{character.eye_color}</td>
                </tr>
                <tr>
                  <th>Homeworld</th>
                  <td>
                    <button onClick={() => showHomeworldModal(character.homeworld)} className="btn btn-dark py-0 px-3">Show</button>
                  </td>
                </tr>
              </tbody>
            </table>
          }
        </div>
      </div>
    </div>
  )
}

export default CharacterDetails
