import { Provider } from 'react-redux';
import type { AppProps } from 'next/app';
import { store } from '../app/store';

import DefaultLayout from '../src/components/layouts/default-layout'

import 'bootstrap/dist/css/bootstrap.css'
import '../styles/globals.scss'

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <Provider store={store}>
      <DefaultLayout>
        <Component {...pageProps} />
      </DefaultLayout>
    </Provider>
  )
}

export default MyApp
