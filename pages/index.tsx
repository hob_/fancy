import type { NextPage } from 'next'
import Link from 'next/link'

const Home: NextPage = () => {
  return (
    <div className="text-center">
      <img src="/images/sw-logo.png" alt="Star Wars logo" className="w-50 d-block mx-auto" />
      <Link href="/characters">
        <a>Entry</a>
      </Link>
    </div>
  )
}

export default Home
