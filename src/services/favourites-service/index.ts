import { Character } from '../../interfaces'

export const findCharacterByName = (characterName: string, characters: Array<Character>): any => characters.find(({ name }) => name === characterName)
export const isFavourite = (characterName: string, favouritesCharacters: Array<Character>): boolean => !!findCharacterByName(characterName, favouritesCharacters)