import { Pages } from '../../interfaces'

export const getPagesNums = (prevLink?: string, nextLink?: string): Pages => {
  return {
    prev: prevLink ? +prevLink.split('page=')[1] : null,
    next: nextLink ? +nextLink.split('page=')[1] : null
  }
}

export const createLinks = (current: number, total: number, additionalLinksNum: number = 3): Array<number> => {
  let start: number | null = null
  let end: number | null = null
  const linksCollection: Array<number> = []

  switch (current) {
    case 1:
      start = 1
      end = start + additionalLinksNum - 1
      break
    case total:
      end = total
      start = end - additionalLinksNum + 1
      break
    default:
      const marginPages = (additionalLinksNum - 1) / 2
      start = current - marginPages
      end = current + marginPages
  }

  if (!start || !end) {
    return linksCollection
  }

  for (let i = start; i <= end; i++) {
    linksCollection.push(i)
  }

  return linksCollection
}
