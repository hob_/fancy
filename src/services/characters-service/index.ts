import axios from 'axios'

const getCharacters = async (pageNum: number | null): Promise<any> => {
  const urlApi = pageNum ? `https://swapi.dev/api/people/?page=${pageNum}` : 'https://swapi.dev/api/people'
  try {
    const { data } = await axios.get(urlApi)
    return data
  } catch (err) {
    return Promise.reject()
  }
}

const getCharacter = async (characterId: number): Promise<any> => {
  try {
    const { data } = await axios.get(`https://swapi.dev/api/people/${characterId}`);
    return data
  } catch (err) {
    return Promise.reject()
  }
}

const getCharacterHomeworld = async (apiUrl: string): Promise<any> => {
  try {
    const { data } = await axios.get(apiUrl);
    return data
  } catch (err) {
    return Promise.reject()
  }
}

export { getCharacters, getCharacter, getCharacterHomeworld }