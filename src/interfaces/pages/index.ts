export interface Pages {
  prev: number | null;
  next: number | null;
}