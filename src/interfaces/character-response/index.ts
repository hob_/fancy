import { Character } from '../character'
interface CharactersResponse {
  count: number;
  next: string | null;
  previous: string | null;
  results: Array<Character>;
}

export type { CharactersResponse }