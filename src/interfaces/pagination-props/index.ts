export interface PaginationProps {
  current: number;
  total: number;
  previous?: string;
  next?: string;
}