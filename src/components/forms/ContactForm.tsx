import { useState, FormEvent } from 'react'
import type { NextPage } from 'next'
import styles from '../../../styles/components/common/ContactForm.module.scss'

const ContactForm: NextPage = () => {
  const [fields, setField] = useState({
    firstName: '',
    lastName: '',
    email: '',
    phone: '',
    postalCode: '',
    message: ''
  })

  const [formErrors, setFormError] = useState([])

  const onSubmit = (event: FormEvent): void => {
    event.preventDefault()
    clearErrors()
    if(isFormValid()) {
      // send data
    }
  }

  const isFormValid = (): boolean => {
    Object.entries(fields).forEach(([fieldKey, fieldValue]) => {
      if (fieldValue.trim() === '') {
        setFormError(arr => [...arr, fieldKey])
        return true
      }
    })

    if (!isEmailValid(fields.email)) {
      setFormError(arr => [...arr, 'email'])
    }

    if (!isPhoneValid(fields.phone)) {
      setFormError(arr => [...arr, 'phone'])
    }

    if (!isPostalCodeValid(fields.postalCode)) {
      setFormError(arr => [...arr, 'postalCode'])
    }

    return !!formErrors.length
  }

  const isEmailValid = (email: string): boolean => /^\S+@\S+\.\S+$/i.test(email)
  const isPhoneValid = (phone: string): boolean => /^[0-9\.\-]{9,11}$/i.test(phone)
  const isPostalCodeValid = (postalCode: string): boolean => /^\d{2}-\d{3}]?[0-9]{4,6}$/i.test(postalCode)


  const clearErrors = (): void => setFormError([])
  const hasError = (key: string): boolean => formErrors.includes(key)

  return (
    <form onSubmit={onSubmit} className={styles['contact-form']}>
      <div className="form-group mb-2">
        <label className={hasError('firstName') ? 'error': ''} htmlFor="firstName">First name *</label>
        <input onChange={(e) => setField({...fields, firstName: e.target.value})} type="text" className="form-control" id="firstName" value={fields.firstName} />
      </div>

      <div className="form-group mb-2">
        <label className={hasError('lastName')? 'error': ''} htmlFor="lastName">Last name *</label>
        <input onChange={(e) => setField({...fields, lastName: e.target.value})} type="text" className="form-control" id="lastName" value={fields.lastName} />
      </div>

      <div className="form-group mb-2">
        <label className={hasError('email') ? 'error': ''} htmlFor="email">Email *</label>
        <input onChange={(e) => setField({...fields, email: e.target.value})} type="email" className="form-control" id="email" value={fields.email} />
      </div>

      <div className="form-group mb-2">
        <label className={hasError('phone') ? 'error': ''} htmlFor="phone">Phone *</label>
        <input onChange={(e) => setField({...fields, phone: e.target.value})} type="tel" className="form-control" id="phone" value={fields.phone} />
      </div>

      <div className="form-group mb-2">
        <label className={hasError('postalCode') ? 'error': ''} htmlFor="postalCode">Postal code *</label>
        <input onChange={(e) => setField({...fields, postalCode: e.target.value})} type="text" className="form-control" id="postalCode" value={fields.postalCode} />
      </div>

      <div className="form-group mb-2">
        <label className={hasError('message') ? 'error': ''} htmlFor="message">Message *</label>
        <textarea onChange={(e) => setField({...fields, message: e.target.value})} className="form-control" id="message" rows="3" value={fields.message}></textarea>
      </div>

      <div className="form-group mb-3">
        <button className="btn btn-dark w-100">Send Form</button>
      </div>
    </form>
  )
}

export default ContactForm
