import PageHead from '../common/page-head'
import MainNavbar from '../common/main-navbar'
import MainFooter from '../common/main-footer'

import styles from '../../../styles/components/layouts/DefaultLayout.module.scss'

export default function DefaultLayout({ children }: any) {
  return (
    <div className={styles['default-layout-container']}>
      <PageHead />
      <MainNavbar />
      <main>{children}</main>
      <MainFooter />
    </div>
  )
}