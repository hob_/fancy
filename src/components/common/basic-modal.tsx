
import { useState } from 'react'
import styles from '../../../styles/components/common/BasicModal.module.scss'
const BasicModal = ({ content, modalTitle, open, hideModal } : { content: string, modalTitle: string, open: boolean, hideModal: any }) => {

  return (
    <div className={open ? `${styles['basic-modal']} ${styles['basic-modal--open']}` : styles['basic-modal']} id="basicModal" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div className="modal-dialog" role="document">
        <div className={`modal-content ${styles['basic-modal__content']}`}>
          <div className="modal-header">
            <h5 className="modal-title" id="exampleModalLabel">{modalTitle}</h5>
          </div>
          <div dangerouslySetInnerHTML={{ __html: content }} className="modal-body"></div>
          <div className="modal-footer">
            <button onClick={() => hideModal()} type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>
  )
}


export default BasicModal
