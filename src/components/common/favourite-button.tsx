import { useState, useEffect } from 'react'
import * as Constants from '../../../constants'
import styles from '../../../styles/components/common/FavouriteButton.module.scss'

const SimpleCharacterList = ({ characterName, isFavourite, clickHandler } : { characterName: string, isFavourite: boolean, clickHandler: (name: string) => void}) => {
  let [src, setSrc] = useState(Constants.INACTIVE_FAVOURITE_ICON_PATH);

  useEffect(() => {
    isFavourite ? setSrc(Constants.ACTIVE_FAVOURITE_ICON_PATH) : setSrc(Constants.INACTIVE_FAVOURITE_ICON_PATH)
  })


  return (
    <button onClick={() => clickHandler(characterName)} className={styles['favourite-button']}>
      <img src={src} alt="Favourites" />
    </button>
  )
}


export default SimpleCharacterList
