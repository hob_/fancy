import Link from 'next/link'
import { useRouter } from 'next/router'
import styles from '../../../styles/components/common/MainNavbar.module.scss'

const MainNavbar = () => {
  const router = useRouter();

  const setDynamicClassLink = (pathname: string, linkPaths: Array<string>, styles: any): string => linkPaths.includes(pathname) ? `${styles['main-navbar__link']} ${styles['main-navbar__link--active']}` : styles['main-navbar__link']

  return (
    <nav className={styles['main-navbar']}>
      <Link href="/">
        <a className={`${setDynamicClassLink(router.pathname, ['/'], styles)}`}>Home</a>
      </Link>
      <Link href="/characters">
        <a className={`${setDynamicClassLink(router.pathname, ['/characters', '/characters/[id]'], styles)}`}>Characters</a>
      </Link>
      <Link href="/favourites">
        <a className={`${setDynamicClassLink(router.pathname, ['/favourites'], styles)}`}>Favourites</a>
      </Link>
      <Link href="/contact">
        <a className={`${setDynamicClassLink(router.pathname, ['/contact'], styles)}`}>Contact</a>
      </Link>
    </nav>
  )
}

export default MainNavbar