import { useEffect } from 'react'
import Link from 'next/link'
import { useAppDispatch, useAppSelector } from '../../../app/hooks'
import { add, addOnlyStore, remove, selectFavourites } from '../../../features/favourites/favouritesSlice'
import { Character } from '../../interfaces'
import { findCharacterByName, isFavourite } from '../../services'

import FavoriteButton from './favourite-button'

import styles from '../../../styles/components/common/SimpleCharacterList.module.scss'

const SimpleCharacterList = ({ collection } : { collection: any }) => {
  const dispatch = useAppDispatch()
  const favouritesList = useAppSelector(selectFavourites)

  const onClickHandler = (characterName: string): void => {
    const selectedCharacter: Character = findCharacterByName(characterName, collection)
    if (isFavourite(characterName, favouritesList)) {
      dispatch(remove(characterName))
    } else {
      dispatch(add([selectedCharacter]))
    }
  }

  const getCharacterIndex = (index: number): number => index + 1

  return (
    <ul className={styles['simple-character-list']}>
      {collection.map((element: Character, index: number) => (
        <li key={index} className={styles['simple-character-list__el']}>
          <Link href={`/characters/${getCharacterIndex(index)}`}>
            {element.name}
          </Link>
          <div className={styles['simple-character-list__actions']}>
            <FavoriteButton characterName={element.name} isFavourite={isFavourite(element.name, favouritesList)} clickHandler={onClickHandler} />
          </div>
        </li>
      ))}
    </ul>
  )
}

export default SimpleCharacterList
