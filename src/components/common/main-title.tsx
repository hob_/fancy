const MainTitle = ({ content }: { content?: string }) => {
  return (
    <h1>{content}</h1>
  )
}

export default MainTitle