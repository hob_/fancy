import Head from 'next/head'

const PageHead = () => {
  return (
    <Head>
      <title>Fancy App</title>
      <meta name="description" content="Fancy App" />
      <link rel="icon" href="/favicon.ico" />
    </Head>
  )
}

export default PageHead