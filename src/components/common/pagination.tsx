import type { NextPage } from 'next'
import Link from 'next/link'
import { PaginationProps } from '../../interfaces'
import styles from '../../../styles/components/common/Pagination.module.scss'
import { getPagesNums, createLinks } from '../../services'

const Pagination: NextPage<PaginationProps> = ({ current, total, previous, next }) => {

  const additionaPaginationLinks: Array<number> = createLinks(current, total)
  const { prev: prevPage, next: nextPage } = getPagesNums(previous, next)

  return (
    <ul className={styles.pagination}>
      <li className={previous ? styles['pagination__list-item'] : `${styles['pagination__list-item']} ${styles['pagination__list-item--disabled']}`}>
        <Link href={`/characters?page=${prevPage}`}>
          <a className={styles['pagination__link']}>Prev</a>
        </Link>
      </li>
      {additionaPaginationLinks.map((linkNum: number) => {
        return (
          <li className="pagination__list-item">
            <Link href={`/characters?page=${linkNum}`}>
              <a className={linkNum === current ? `${styles['pagination__link']} ${styles['pagination__link--active']}` : styles['pagination__link']}>{linkNum}</a>
            </Link>
          </li>
        )
      })}
      
      <li className={next ? styles['pagination__list-item'] : `${styles['pagination__list-item']} ${styles['pagination__list-item--disabled']}`}>
        <Link href={`/characters?page=${nextPage}`}>
          <a className={styles['pagination__link']}>Next</a>
        </Link>
      </li>
    </ul>
  )
}

export default Pagination