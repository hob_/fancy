import styles from '../../../styles/components/common/MainFooter.module.scss'

const MainFooter = () => {
  return (
    <footer className={styles['main-footer']}>Footer</footer>
  )
}

export default MainFooter