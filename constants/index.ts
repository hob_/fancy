export const FAVOURITES_KEY: string = 'favourites'
export const ACTIVE_FAVOURITE_ICON_PATH: string = '/images/heart-red.svg'
export const INACTIVE_FAVOURITE_ICON_PATH: string = '/images/heart.svg'
